## Third task: Contacts handler

**Technologies used:**

1. RecyclerView
2. ContactsContract
3. DialogFragment
4. SharedPreferences
5. Notification  
6. Styles

<img alt="TransitionsDemo" src="https://bitbucket.org/g0development-admin/android-school-third-task/src/dev/ContactsDemo.gif"/>